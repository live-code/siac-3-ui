import { Component } from '@angular/core';
import { map, Subscription } from 'rxjs';
import { AuthService } from './core/auth.service';

@Component({
  selector: 'app-root',
  template: `
    <div class="flex gap-2">
      <button routerLink="login" appRouterLinkActive="active">login</button>
      <button routerLink="demo1" appRouterLinkActive="active">demo1</button>
      <button routerLink="demo2" routerLinkActive="active">demo2</button>
      <button routerLink="demo3" appRouterLinkActive="active">demo3</button>
      <button routerLink="demo4" routerLinkActive="active">demo4</button>
      <button routerLink="demo5" routerLinkActive="active">demo5</button>
      <button
        routerLink="demo6" routerLinkActive="active">demo6</button>
      
  
      <button
        *appIfSignin="'admin'"
        (click)="authServ.logout()"
      >logout</button>
    
    </div>
    <hr>  
    <div class="m-3">
      <router-outlet></router-outlet>
    </div>

  `,
  styles: [`
    button { 
      @apply bg-sky-500
    }
    .active {
      @apply bg-sky-200
    }
  `]

})
export class AppComponent {
 constructor(public authServ: AuthService) {  }
}
