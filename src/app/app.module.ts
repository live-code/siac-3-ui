import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { environment } from '../environments/environment';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FakeLogService } from './core/fake-log.service';
import { LogService } from './core/log.service';
import { CardComponent } from './shared/components/card.component';
import { SeparatorComponent } from './shared/components/separator.component';
import { SharedModule } from './shared/shared.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    SharedModule,
  ],
providers: [
    {
      provide: LogService,
      useFactory: () => {
        return environment.production ?
          new LogService() : new FakeLogService()
      }
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

