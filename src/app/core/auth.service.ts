import { Injectable } from '@angular/core';
import { BehaviorSubject, map } from 'rxjs';
import { Auth } from '../model/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  auth$ = new BehaviorSubject<Auth | null>(null)

  login() {
    const res: Auth = {
      token: 'abc123',
      displayName: 'Fabio',
      role: 'admin'
    }
    this.auth$.next(res)
  }

  logout() {
    this.auth$.next(null)
  }

  get ifLogged$() {
    return this.auth$
      .pipe(
        map(auth => !!auth)
      )
  }

  get role$() {
    return this.auth$
      .pipe(
        map(auth => auth?.role)
      )
  }

  get token() {
    return 'efwf'
  }
}
