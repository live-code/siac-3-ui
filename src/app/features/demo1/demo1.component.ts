import { Component } from '@angular/core';
import { LogService } from '../../core/log.service';

@Component({
  selector: 'app-demo1',
  template: `

    <app-title size="1">Animated Toggle</app-title>
    <app-toggle
      icon="fa fa-google"
      title="profile"
      (iconClick)="openLink()"
    >
      <input type="text">
      <input type="text">
    </app-toggle>

    <app-toggle
      icon="fa fa-google"
      title="profile"
      (iconClick)="openLink()"
    >
      <app-mapquest city="Palermo"></app-mapquest>
    </app-toggle>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    
    <app-title size="1">Static Map</app-title>
    <app-mapquest [city]="myCity" [zoom]="myZoom"></app-mapquest>
    
    <app-button-group [gap]="2">
      <app-button (click)="myCity = 'palermo'">palermo</app-button>
      <app-button (click)="myCity = 'bari'">Bari</app-button>
      <app-button variant="alert" (click)="myZoom = myZoom + 1">+</app-button>
      <app-button variant="alert" (click)="myZoom = myZoom - 1">-</app-button>
    </app-button-group>
    
    <app-title size="1">Card</app-title>
    
    <app-card 
      icon="fa fa-google" 
      title="profile"
      (iconClick)="openLink()"
    >
      <input type="text">
      <input type="text">
    </app-card>
    
    <app-card 
      icon="fa fa-eye" 
      title="profile"
      (iconClick)="visible = true"
    >
      <input type="text">
    </app-card>
    
    <div *ngIf="visible">ciao</div>
  `,
})
export class Demo1Component {
  visible = false;
  myCity = 'Trieste'
  myZoom = 10;


  constructor(private logService: LogService) {
    logService.message()
  }

  openLink() {
    window.open('http://www.google.com')
  }
}
