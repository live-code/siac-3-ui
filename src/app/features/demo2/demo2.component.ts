import { Component } from '@angular/core';

export interface Country {
  id: number;
  label: string;
  desc: string;
  cities: City[]
}

export interface City {
  id: number;
  name: string
}


@Component({
  selector: 'app-demo2',
  template: `
    <app-title>Tab Bar demo</app-title>

    <app-tab-bar
      [items]="countries"
      [active]="selectedCountry"
      (itemClick)="setActiveCountry($event)"
    ></app-tab-bar>

    
    <app-tab-bar
      *ngIf="selectedCountry"
      [active]="selectedCity"
      labelField="name"
      [items]="selectedCountry.cities"
      (itemClick)="setActiveCity($event)"
    ></app-tab-bar>

    <app-mapquest
      *ngIf="selectedCity"
      [city]="selectedCity.name"/>
  `,
})
export class Demo2Component {
  selectedCountry: Country | undefined;
  selectedCity: City | undefined;

  setActiveCountry(item: Country) {
    this.selectedCountry = item;
    this.selectedCity = this.selectedCountry.cities[0]

  }

  setActiveCity(item: City) {
    this.selectedCity = item;
  }

  countries: Country[] = []

  constructor() {
    setTimeout(() => {
      this.countries = [
        {
          id: 2,
          label: 'germany',
          desc: 'bla bla 2',
          cities: [
            { id: 1, name: 'Berlin' },
            { id: 2, name: 'Monaco' }
          ]
        },
        {
          id: 1,
          label: 'italy',
          desc: 'bla bla 1',
          cities: [
            { id: 11, name: 'Rome' },
            { id: 22, name: 'Milan' },
            { id: 33, name: 'Palermo' },
          ]
        },
        { id: 3, label: 'spain', desc: 'bla bla 3', cities: [
            {id: 41, name: 'Madrid'}
          ]},
      ];

      this.selectedCountry = this.countries[0]
      this.selectedCity = this.selectedCountry.cities[0]
    }, 200)
  }

}
