import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from '../../shared/components/card.component';
import { SharedModule } from '../../shared/shared.module';

import { Demo2RoutingModule } from './demo2-routing.module';
import { Demo2Component } from './demo2.component';


@NgModule({
  declarations: [
    Demo2Component,
  ],
  imports: [
    SharedModule,
    CommonModule,
    Demo2RoutingModule
  ]
})
export class Demo2Module { }
