import { Component } from '@angular/core';
import { FakeLogService } from '../../core/fake-log.service';
import { LogService } from '../../core/log.service';

@Component({
  selector: 'app-demo3',
  template: `
    <app-title>Accordion</app-title>

    <app-accordion>
      <app-group title="one"> bla bla </app-group>
      <app-group title="two"> bla bla </app-group>
      <app-group title="three"> bla bla </app-group>
    </app-accordion>

    <app-title>Grid</app-title>


    <app-fx gap="lg" align="between">
      <app-col bg="red">one</app-col>
      <app-col bg="green">two</app-col>
      <app-col bg="cyan">three</app-col>
    </app-fx>

    
    
    <app-title>Leaflet</app-title>
    <app-leaflet
      [coords]="myCoords"
      [zoom]="myZoom"
      [enableCircle]="circle"
    ></app-leaflet>
    
    <button (click)="myCoords = [42, 11]">coords 1</button>
    <button (click)="myCoords = [44, 14]">coords 2</button>
    <button (click)="myZoom = myZoom + 1">+</button>
    <button (click)="myZoom = myZoom - 1">-</button>
    <button (click)="circle = true">circle on</button>
    <button (click)="circle = false">circle off</button>
    
  `,

})
export class Demo3Component {
  myCoords: [number, number] = [43, 13]
  myZoom = 13;
  circle = true;


}
