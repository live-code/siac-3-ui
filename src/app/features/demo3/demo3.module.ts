import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FakeLogService } from '../../core/fake-log.service';
import { LogService } from '../../core/log.service';
import { SharedModule } from '../../shared/shared.module';

import { Demo3RoutingModule } from './demo3-routing.module';
import { Demo3Component } from './demo3.component';


@NgModule({
  declarations: [
    Demo3Component
  ],

  imports: [
    CommonModule,
    Demo3RoutingModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class Demo3Module { }
