import { Component } from '@angular/core';

@Component({
  selector: 'app-demo4',
  template: `
    <app-title>Bg Directive</app-title>
    <input type="text" [(ngModel)]="color" placeholder="change color">
    <div [appBg]="color">bg directive</div>
    
    <app-title>Pad Directive</app-title>
    <div appPad="sm" margin="xl">element with padding</div>

    <app-title>Highight Directive</app-title>
    Hello <span appHighlight>world</span><span appHighlight>world</span>
    
    <app-title>Alert Directive</app-title>
    <div appAlert="success">alert 1!</div>
    <span appAlert="failed">alert 2!</span>

    <app-title>Url directive</app-title>
    
    Hello <span appUrl="http://www.google.com">
      google
    </span>
    
    <button  appUrl="http://www.fabiobiondi.dev">my site</button>
 
    <div (click)="parent()" class="bg-sky-500 p-3">
      <div (click)="child()" appStopPropagation class="bg-orange-300 p-3"></div>
    </div>
    
    
  `,
})
export class Demo4Component {
  color = '#ff0000'

  parent() {
    console.log('parent')
  }

  child() {
    console.log('child')
  }
}
