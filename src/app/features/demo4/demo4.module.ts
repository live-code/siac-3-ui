import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';

import { Demo4RoutingModule } from './demo4-routing.module';
import { Demo4Component } from './demo4.component';


@NgModule({
  declarations: [
    Demo4Component
  ],
  imports: [
    CommonModule,
    Demo4RoutingModule,
    SharedModule,
    FormsModule
  ]
})
export class Demo4Module { }
