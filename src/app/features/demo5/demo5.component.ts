import { Component, TemplateRef, ViewChild, ViewContainerRef } from '@angular/core';

@Component({
  selector: 'app-demo5',
  template: `
    <ng-template #content>
      dynamic content
    </ng-template>

    <p *ngIf="visible; else welcomeMsg">
      demo5 works!
    </p>
    
    <ng-template #welcomeMsg>
      welcome pippo!
    </ng-template>
    
    <hr>
    <h1>Hello</h1>
  `,
})
export class Demo5Component {
  @ViewChild('content') myContent!: TemplateRef<any>;

  visible = false;

  constructor(
    private view: ViewContainerRef
  ) {
    setTimeout(() => {
      view.createEmbeddedView(this.myContent)
      view.createEmbeddedView(this.myContent)
      view.createEmbeddedView(this.myContent)
      view.createEmbeddedView(this.myContent)
      this.visible = true;
    }, 2000)
  }
}
