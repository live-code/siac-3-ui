import { Component } from '@angular/core';
import { AuthService } from '../../core/auth.service';

@Component({
  selector: 'app-login',
  template: `
    <p>
      login simulator
    </p>
    
    <button (click)="authService.login()">
      SIGN IN BUTTON
    </button>
  `,
  styles: [
  ]
})
export class LoginComponent {

  constructor(public authService: AuthService) {
  }
}
