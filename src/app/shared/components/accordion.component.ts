import { AfterContentInit, Component, ContentChildren, QueryList, ViewChildren } from '@angular/core';
import { GroupComponent } from './group.component';

@Component({
  selector: 'app-accordion',
  template: `
    <div style="border: 1px solid black">
      <ng-content></ng-content>
    </div>
  `,
})
export class AccordionComponent implements AfterContentInit {
  @ContentChildren(GroupComponent) groups!: QueryList<GroupComponent>;

  ngAfterContentInit() {
    this.groups.first.isOpen = true;

    this.groups.toArray().forEach(g => {
      g.headerClick.subscribe(() => {
        this.closeAll()
        g.isOpen = true;
      })
    })
  }

  closeAll() {
    this.groups.toArray().forEach(g => {
      g.isOpen = false;
    })
  }
}
