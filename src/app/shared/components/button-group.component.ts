import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-button-group',
  template: `
    <div 
      class="flex"
      [ngClass]="{
        'gap-1': gap === 1,
        'gap-2': gap === 2,
        'gap-4': gap === 4,
        'gap-8': gap === 8,
      
      }"
    >
      <ng-content></ng-content>
    </div>
  `,
})
export class ButtonGroupComponent {
  @Input() gap: 1 | 2 | 4 | 8 = 1;
}

