import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-button',
  template: `
    <button [ngClass]="{
      'primary': variant === 'primary',
      'alert': variant === 'alert',
      'border-2 border-black pointer-events-none': selected,
    }">
      <ng-content></ng-content>
    </button>
  `,
  styles: [`
    button {
      @apply  px-3 py-1 rounded-xl  transition duration-500 disabled:opacity-25
    }

    button.primary {
      @apply bg-sky-400 hover:bg-sky-800 hover:text-white
    }

    button.alert {
      @apply bg-red-400 hover:bg-red-800 hover:text-white
    }
  `]
})
export class ButtonComponent {
  @Input() variant: 'primary' | 'alert' = 'primary'
  @Input() selected: boolean = false;
}
