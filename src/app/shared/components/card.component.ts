import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-card',
  template: `
    <div>
      <h1
        class="title" *ngIf="title"
        (click)="isOpen = !isOpen"
      >
        <div>{{title}}</div>
        <i 
          *ngIf="icon"
          [class]="icon"
          (click)="iconClickHandler($event)"
        ></i>
      </h1>
      
      <div class="body" *ngIf="isOpen">
        <ng-content></ng-content>
      </div>
    </div>
  `,
  styles: [`
      .title {
        @apply bg-slate-800 text-white p-3 flex justify-between
      }
      
      .body {
        @apply border-2 border-slate-800 p-3
      }
  `]
})
export class CardComponent {
  @Input() title: string | undefined
  @Input() icon: string | undefined
  @Input() url: string | undefined

  @Output() iconClick = new EventEmitter()

  isOpen = true;

  iconClickHandler(e: MouseEvent) {
    e.stopPropagation();
    this.iconClick.emit()
  }
}
