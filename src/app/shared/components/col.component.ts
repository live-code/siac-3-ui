import { Component, HostBinding, Input } from '@angular/core';

@Component({
  selector: 'app-col',
  template: `
    <ng-content></ng-content>
  `,
})
export class ColComponent {
  @Input() bg: string | undefined;
  @HostBinding() class = 'grow'
  @HostBinding('style.background') get background() {
    return this.bg;
  }
}
