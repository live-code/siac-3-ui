import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-fx',
  template: `
    <div class="flex" [ngClass]="{
      'gap-0': !gap,
      'gap-1': gap === 'xs',
      'gap-4': gap === 'md',
      'gap-8': gap === 'lg',
      'justify-between': align === 'between',
      'justify-end': align === 'end'
    }">
      <ng-content></ng-content>
    </div>
  `,
  styles: [
  ]
})
export class FxComponent {
  @Input() gap: 'xs' | 'md' | 'lg' | undefined;
  @Input() align: 'between' | 'end' | undefined;
}
