import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-group',
  template: `
    <div>
      <h1
        class="title" *ngIf="title"
        (click)="headerClick.emit()"
      >
        <div>{{title}}</div>
        <i
          *ngIf="icon"
          [class]="icon"
          (click)="iconClickHandler($event)"
        ></i>
      </h1>

      <div class="body" *ngIf="isOpen">
        <ng-content></ng-content>
      </div>
    </div>
  `,
  styles: [`
      .title {
        @apply bg-slate-800 text-white p-3 flex justify-between
      }
      
      .body {
        @apply border-2 border-slate-800 p-3
      }
  `]
})
export class GroupComponent {
  @Input() title: string | undefined
  @Input() icon: string | undefined
  @Input() url: string | undefined

  @Output() headerClick = new EventEmitter()
  @Output() iconClick = new EventEmitter()

  @Input() isOpen = false;

  iconClickHandler(e: MouseEvent) {
    e.stopPropagation();
    this.iconClick.emit()
  }
}
