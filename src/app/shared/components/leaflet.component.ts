import { AfterViewInit, Component, ElementRef, Input, SimpleChanges, ViewChild } from '@angular/core';
import { LatLngExpression } from 'leaflet';
import * as L from 'leaflet';
import * as LeafLetUtils from './leaflet.utils';

const markerIcon = L.icon({
  iconUrl: '../assets/marker-icon.png',
  iconSize:     [25, 41],
  iconAnchor:   [12, 41],
  popupAnchor:  [0, -50]
});

@Component({
  selector: 'app-leaflet',
  template: `
    <div #host class="map" ></div>
  `,
  styles: [`
    .map { height: 180px; }
  `]
})
export class LeafletComponent {
  @ViewChild('host', { static: true}) host!: ElementRef<HTMLElement>;
  @Input() coords!: LatLngExpression
  @Input() enableCircle: boolean = false;
  @Input() zoom: number = 5;

  map!: L.Map;
  marker!: L.Marker;
  circle!: L.Circle;


  ngOnChanges(changes: SimpleChanges) {
    if (!this.map) {
      this.map = LeafLetUtils.initMap(this.host.nativeElement)
      this.marker = L.marker(this.coords, { icon: markerIcon }).addTo(this.map);
    }
    if (this.map) {
      if (changes['zoom']) {
        this.map.setZoom(this.zoom)
      }
      if (changes['coords']) {
        this.map.setView(this.coords)
        this.marker.setLatLng(this.coords)
      }
      if (changes['enableCircle']) {
        this.drawCircle()
      }
    }
  }

  drawCircle() {
    if (this.enableCircle) {
      this.circle = LeafLetUtils.drawCircle(this.coords, this.map)
    } else {
      LeafLetUtils.removeCircle(this.circle, this.map)
    }
  }
}


