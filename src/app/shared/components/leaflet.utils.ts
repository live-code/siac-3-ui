

// leaflet.utils.js
import * as L from 'leaflet';

export function drawCircle(coords: any, map: any) {
  return L.circle(coords, {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 500
  }).addTo(map);
}
export function removeCircle(circleRef: L.Circle, map: L.Map) {
  map.removeLayer(circleRef)
}

export function initMap(element: HTMLElement) {
  const map = L.map(element).setView([11, 11], 4);

  L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
  }).addTo(map);

  return map;
}
