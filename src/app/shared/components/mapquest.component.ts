import { Component, Input, OnChanges, OnInit } from '@angular/core';

const BASEURL = 'https://www.mapquestapi.com/staticmap/v5/map'
const TOKEN = 'Go3ZWai1i4nd2o7kBuAUs4y7pnpjXdZn';

@Component({
  selector: 'app-mapquest',
  template: `
    <img [src]="url" alt="">
  `,
})
export class MapquestComponent implements OnChanges {
  @Input({ required: true }) city: string = ''
  @Input() zoom: number = 10
  url: string | undefined

  ngOnChanges() {
    this.url = `${BASEURL}?key=${TOKEN}&size=400,300&zoom=${this.zoom}&center=${this.city}`
  }
}
