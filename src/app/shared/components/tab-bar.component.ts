import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Country } from '../../features/demo2/demo2.component';

@Component({
  selector: 'app-tab-bar',
  template: `
    
    <app-button-group>
      <app-button
        *ngFor="let item of items"
        [selected]="item.id === active?.id"
        (click)="itemClick.emit(item)"
      >{{item[labelField]}}</app-button>
    </app-button-group>
    
  `,
})
export class TabBarComponent<T extends { id: number, [key: string]: any}> {
  @Input() items: T[] = [];
  @Input() active: T | undefined;
  @Input() labelField = 'label';
  @Output() itemClick = new EventEmitter<T>()
}


