import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-title',
  template: `
    <h1 [ngClass]="{
      'size1': size === '1',
      'size2': size === '2',
      'size3': size === '3',
      'mt-16': margin
    }">
      <ng-content></ng-content>
    </h1>
  `,
  styles: [`
    .size1 {
      @apply text-5xl
    }
    .size2 {
      @apply text-4xl
    }
    .size3 {
      @apply text-3xl
    }
  `]
})
export class TitleComponent {
  @Input() size: '1' | '2' | '3' = '1';
  @Input() margin = false;
}
