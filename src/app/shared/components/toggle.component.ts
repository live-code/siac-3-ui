import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-toggle',
  animations: [
    trigger('collapse', [
      state('opened', style({ height: '*', padding: 10 })),
      state('closed', style({ height: 0, padding: 0})),
      transition('opened => closed', [animate('0.7s ease-in-out')]),
      transition('closed => opened', [animate('2.7s ease-in-out')])
    ])
  ],
  template: `
    <div>
      <h1
        class="title" *ngIf="title"
        (click)="isOpen = !isOpen"
      >
        <div>{{title}}</div>
        <i 
          *ngIf="icon"
          [class]="icon"
          (click)="iconClickHandler($event)"
        ></i>
      </h1>
      
      <div class="body" [@collapse]="isOpen ? 'opened' : 'closed'">
        <ng-content></ng-content>
      </div>
    </div>
  `,
  styles: [`
      .title {
        @apply bg-slate-800 text-white p-3 flex justify-between
      }
      
      .body {
        @apply border-2 border-slate-800 p-3 overflow-hidden;
        
        transition: all 1s ease-in-out;
      }
    /*  .close {
        padding: 0;
        overflow: hidden;
      }*/
  `]
})
export class ToggleComponent {
  @Input() title: string | undefined
  @Input() icon: string | undefined
  @Input() url: string | undefined

  @Output() iconClick = new EventEmitter()

  isOpen = true;

  iconClickHandler(e: MouseEvent) {
    e.stopPropagation();
    this.iconClick.emit()
  }
}
