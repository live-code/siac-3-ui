import { Directive, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[appAlert]'
})
export class AlertDirective {
  @HostBinding() get class(){
    if (this.appAlert === 'success') {
      return 'bg-green-500 p-3 text-white rounded-xl'
    }
    return 'bg-red-500 p-3 text-white rounded-xl'
  }
  @Input() appAlert!: 'success' | 'failed'

}
