import { Directive, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[appBg]'
})
export class BgDirective {
  @HostBinding('style.background') get background() {
    return this.appBg
  }
  @Input() appBg: string | undefined;

  constructor() {
  }
}
