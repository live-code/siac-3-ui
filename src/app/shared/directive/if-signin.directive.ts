import { Directive, ElementRef, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { distinctUntilChanged, map, Subscription, tap, withLatestFrom } from 'rxjs';
import { AuthService } from '../../core/auth.service';

@Directive({
  selector: '[appIfSignin]'
})
export class IfSigninDirective {
  @Input() appIfSignin: 'admin' | 'moderator' | 'guest' | undefined;
  sub!: Subscription

  constructor(
    public authServ: AuthService,
    private el: ElementRef<HTMLElement>,
    private tpl: TemplateRef<any>,
    private view: ViewContainerRef
  ) {}

  ngOnInit() {
    this.authServ.ifLogged$
      .pipe(
        distinctUntilChanged(),
        withLatestFrom(this.authServ.role$)
      )
      .subscribe(([logged, role]) => {
        if (this.appIfSignin === role) {
          this.view.createEmbeddedView(this.tpl)
        } else {
          this.view.clear()
        }
      })
  }

  ngOnDestroy() {
    this.sub.unsubscribe()
  }

}
