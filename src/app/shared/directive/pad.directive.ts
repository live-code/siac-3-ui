import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appPad]'
})
export class PadDirective {
  @Input() set appPad(val: 'sm' | 'xl') {
    this.renderer.setStyle(
      this.elementRef.nativeElement,
      'padding',
      val === 'sm' ? '10px' : '40px'
    )
  }

  @Input() set margin(val: 'sm' | 'xl') {
    this.renderer.setStyle(
      this.elementRef.nativeElement,
      'margin',
      val === 'sm' ? '10px' : '40px'
    )
  }

  constructor(
    private elementRef: ElementRef<HTMLElement>,
    private renderer: Renderer2,
  ) {
  }

}

