import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';

@Directive({
  selector: '[appRouterLinkActive]'
})
export class RouterLinkActiveDirective {
  @Input() appRouterLinkActive!: string;

  constructor(
    private el: ElementRef<HTMLElement>,
    private renderer: Renderer2,
    private router: Router
  ) {
    const routerLink = el.nativeElement.getAttribute('routerLink')

    router.events
      .subscribe(event => {
        if(event instanceof NavigationEnd) {
          if(event.url.includes(routerLink!)) {
            // match
            this.renderer.addClass(el.nativeElement, this.appRouterLinkActive)
          } else {
            // dont match
            this.renderer.removeClass(el.nativeElement, this.appRouterLinkActive)

          }
        }
      })
  }
}
