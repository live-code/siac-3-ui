import { Directive, ElementRef, HostBinding, HostListener, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appUrl]'
})
export class UrlDirective {
  @Input() appUrl!: string;
  @HostBinding('style.cursor') cursor = 'pointer'
  @HostBinding('style.color') color = 'orange'

  @HostListener('click')
  clickMe() {
    window.open(this.appUrl)
  }

  constructor(
    private el: ElementRef<HTMLElement>,
    private renderer: Renderer2
  ) {

  }

}
