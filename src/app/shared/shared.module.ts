import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardComponent } from './components/card.component';
import { SeparatorComponent } from './components/separator.component';
import { TitleComponent } from './components/title.component';
import { MapquestComponent } from './components/mapquest.component';
import { ButtonComponent } from './components/button.component';
import { ButtonGroupComponent } from './components/button-group.component';
import { TabBarComponent } from './components/tab-bar.component';
import { ToggleComponent } from './components/toggle.component';
import { LeafletComponent } from './components/leaflet.component';
import { AccordionComponent } from './components/accordion.component';
import { GroupComponent } from './components/group.component';
import { FxComponent } from './components/fx.component';
import { ColComponent } from './components/col.component';
import { BgDirective } from './directive/bg.directive';
import { AlertDirective } from './directive/alert.directive';
import { HighlightDirective } from './directive/highlight.directive';
import { PadDirective } from './directive/pad.directive';
import { UrlDirective } from './directive/url.directive';
import { StopPropagationDirective } from './directive/stop-propagation.directive';
import { RouterLinkActiveDirective } from './directive/router-link-active.directive';
import { IfLoggedDirective } from './directive/if-logged.directive';
import { IfSigninDirective } from './directive/if-signin.directive';

@NgModule({
  declarations: [
    CardComponent,
    SeparatorComponent,
    TitleComponent,
    MapquestComponent,
    ButtonComponent,
    ButtonGroupComponent,
    TabBarComponent,
    ToggleComponent,
    LeafletComponent,
    AccordionComponent,
    GroupComponent,
    GroupComponent,
    FxComponent,
    ColComponent,
    BgDirective,
    AlertDirective,
    HighlightDirective,
    PadDirective,
    UrlDirective,
    StopPropagationDirective,
    RouterLinkActiveDirective,
    IfLoggedDirective,
    IfSigninDirective
  ],
  imports: [
    CommonModule
  ],
  exports: [
    CardComponent,
    SeparatorComponent,
    TitleComponent,
    MapquestComponent,
    ButtonComponent,
    ButtonGroupComponent,
    TabBarComponent,
    ToggleComponent,
    LeafletComponent,
    AccordionComponent,
    GroupComponent,
    GroupComponent,
    FxComponent,
    ColComponent,
    BgDirective,
    AlertDirective,
    HighlightDirective,
    PadDirective,
    UrlDirective,
    StopPropagationDirective,
    RouterLinkActiveDirective,
    IfLoggedDirective,
    IfSigninDirective
  ]
})
export class SharedModule {}
